# Welcome To Git

Git is a popular open source version control system. It is designed to make collaborating with code easy and robust. 

[Wikipedia](https://en.wikipedia.org/wiki/Git)

## Setup
*This section only applies if using the srcLogic Training VM. Otherwise, refer to the internet for platform specific instructions*

Git is already installed on your Training VM. To set it up for your usage, first register an account at [Gitlab](https://gitlab.com) using your srcLogic email address as your username. Once you have done that, you can run the below two commands to configure Git in your VM.

```
$ git config --global user.name "Your name here"
$ git config --global user.email YourEmailHere@srclogic.com
```

This will tell Git what values to use as you work with files within a git tracked system.

## Basic Usage
Covering the basic Git commands you will be expected to know. While this guide is specific to Gitlab, the terminal commands are universal for any Git project you use later on.

### Clone
First, we will clone a basic repository, this one to be precise. Cloning in Git simply replicates the contents of the online repository into your local machine's file system. The benefit of cloning though creates connections back to the source repository so you as developers can manipulate files within the repository in your local environment before pushing those changes back to the source.

Since you're reading this, you're already on the home screen of this repository. Above this panel should be a blue button labeled "Clone". Click it, and you will get a small pop up with two URLs. We will ignore the first, but copy the second URL under the heading "Clone with HTTPS". Then, open a terminal session and navigate to the directory you want to clone the repository into. I recommend `~/apps`. Once in your target directory, run the below command:
```
$ git clone <repositoryURL>
```

Git will then download the contents of this repoisotrepository into a new directory. Now you have a copy of the repoistory, what can you do with it?

### Add
The "add" command tells Git to add an edited file to the next [commit](#commit). Git will automatically take note of any change you make to a file in the repository but you need to use the "add" command to tell Git that this change, this point in time snapshot of a changed file should be added to the next [commit](#commit). There are two ways to use the "add" command:

```
# to add an individual file
$ git add /path/to/file

# or to add all changed files as they currently are
$ git add -A
```

Remember, the "add" command only adds the current version of the file to the next [commit](#commit). Any changes you make to the file need to be re-added in order to commit them.

### Commit
Git commits can be thought of as markes within the development of a codebase. An indvidual commit contains all the changes to the repository between the last commit and the time the current commit is created. The beauty of Git and what makes it so useful is that any individual commit contains only the information about itself and the a link to the previous one, forming a chain. This chain can then be used to move through the history of a repository as one can simple navigate to a given commit and Git will pull revert the repository to the point in time where that commit was made.

To use commits is simple. Once you have added one of more changed files to the commit, the command to create that commit, that snapshot of the changes since the last commit is:

```
$ git commit -m "Your message here"
```

The commit message, what gets placed within the quotes in the command is an important part of the commit as it tells both you and any developers you work with what the point of that commit is. Did you fix a bug? Did you add a new feature? It's a way to facilitate communication between different developers working in the same repository.

### Push
Making a commit is great but now you have to share it. The "push" command tells Git take all the commits you've made that aren't at the source repository (in the cloud) and send them there. This is how you backup your work to a remote system and how you share it with your collegues as they can now [pull](#pull) your changes down to their version of the repository and incorporate your work into theirs. 
```
$ git push
```
Remember, this command will take all commits you've made since the last push and send them to the remote system. You don't need to run the command for every commit unless it makes sense to share it depending on your workflow. 

### Pull

Pull is the opposite of [push](#push). The pull command will look at the remote repository and pull any new commits into your local environment and update the relevant sections. This allows you to synchronize your local environment with the remote environment. See [Merge Conflicts](#merge-conflicts) for when this can go slightly wrong.

```
$ git pull
```

### Blame
Whodoneit

## Common Issues
Messing up your Git repository is a rite-of-passage for all developers. You will do it, and that's ok.


### Merge Conflicts
A merge conflict occurs when two commits created seperately involve the same line or lines of code. Many code editors provide an easy to use interface to help resolve these. It's a common issue and simply requires editing the relevant files to resolve the conflict.

Git tracks changes on a line by line basis so it's very easy for conflicts to arise when multiple developers are working together. A common solution is to frequently run `$ git pull` as other developers create and push commits. 

### Deleting Something You Weren't Supposed To
It happens. As long as you haven't made a commit it's a very simple thing to do.
```
# to un-add / un-stage all changes
$ git reset

# or for a single file
$ git reset /path/to/my/file
```
Git tracks deleting a file just as it tracks changing a file so you can simply un-stage the change and the file will be back.

If you've made a commit, it can be more difficult.
```
$ git reset HEAD~
```
This command will revert to the previous previous commit so you may end up losing other work within that commit. 